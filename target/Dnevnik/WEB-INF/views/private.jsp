<!Doctype html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<head>
    <spring:url value="resources/css/bootstrap.css" var="bootstrap"/>
    <spring:url value="resources/css/bootstrap-grid.css" var="bootstrap_grid"/>
    <spring:url value="resources/css/bootstrap-reboot.css" var="bootstrap_reboot"/>
    <spring:url value="resources/js/bootstrap.js" var="bootstrap_script"/>
    <spring:url value="resources/js/bootstrap.bundle.js" var="bootstrap_bundle_script"/>
    <link href="${bootstrap}" rel="stylesheet" />
    <link href="${bootstrap_grid}" rel="stylesheet" />
    <link href="${bootstrap_reboot}" rel="stylesheet" />
    <script src="${bootstrap_script}"></script>
    <script src="${bootstrap_bundle_script}"></script>
    <title>Private Page</title>
    <style>
        .right-margin{
            margin-right: 6%;
        }
    </style>
</head>
<body>
<nav class="navbar navbar-expand navbar-light bg-light">
    <a class="navbar-brand" href="/main">StudentDnevnik</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarText">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="PayPal.Me/maramzin">Donate Us</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/signin">Resign In</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Feedback</a>
            </li>
        </ul>
        <ul class="navbar-nav rounded-left right-margin">
            <li class="nav-item active">
                <a class="nav-link" href="#">User<span class="sr-only">(current)</span></a>
            </li>
        </ul>
        <form class="form-inline">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>
    </div>
</nav>
<div class="accordion" id="accordionExample">
    <div class="card">

        <div class="card-header" id="headingOne">
            <h2 class="mb-0">
                Текущая успеваемость:
            </h2>
        </div>

        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
            <div class="card-body">
                <table class="table">
                    <tmain>
                        <tr>
                            <th scope="col">Subject</th>
                            <th scope="col">Number Of Points</th>
                            <th scope="col">Possible Estimate</th>
                        </tr>
                    </tmain>
                    <tbody>
                        <tr>
                            <th scope="col">Теория вероятностей</th>
                            <th scope="col">54</th>
                            <th scope="col">3</th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</body>
</html>
