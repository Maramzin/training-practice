package org.example.service;

import org.example.domain.User;
import org.example.repository.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("userInfoService")
public class UserInfoServiceImpl implements UserInfoService {

    @Autowired
    UserDAO userDAO;

    @Transactional
    @Override
    public User getUserByUsername(String username) {
        return userDAO.findUserByUsername(username);
    }
}
