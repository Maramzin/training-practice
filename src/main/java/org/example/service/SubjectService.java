package org.example.service;

import org.example.domain.Subject;

import java.util.List;

public interface SubjectService {
    Subject findSubjectById(int id);
    List<Subject> getAll();
}
