package org.example.service;

import org.example.domain.Subject;
import org.example.repository.SubjectDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("subjectService")
public class SubjectServiceImpl implements SubjectService {

    @Autowired
    SubjectDAO subjectDAO;

    @Transactional
    @Override
    public Subject findSubjectById(int id) {
        return subjectDAO.getSubjectById(id);
    }

    @Transactional
    @Override
    public List<Subject> getAll() {
        return subjectDAO.getAll();
    }
}
