package org.example.service;

import org.example.domain.User;

public interface UserInfoService {
    User getUserByUsername(String username);
}
