package org.example.controller;

import org.example.domain.Role;
import org.example.domain.Subject;
import org.example.domain.User;
import org.example.service.SubjectService;
import org.example.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;
import java.util.List;

@Controller
public class MainController {
    private UserDetailsService userDetailsService;
    private UserInfoService userInfoService;
    private SubjectService subjectService;

    @Autowired
    public void setSubjectService(SubjectService subjectService) {
        this.subjectService = subjectService;
    }

    @Autowired
    public void setUserDetailsService(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @Autowired
    public void setUserInfoService(UserInfoService userInfoService) {
        this.userInfoService = userInfoService;
    }


    @GetMapping("/")
    public String index(Model model, Principal principal) {
        User user = userInfoService.getUserByUsername(principal.getName());
        model.addAttribute("message", user.getRealname());
        model.addAttribute("user", user);
        List<Subject> allSubjects = subjectService.getAll();
        Boolean isStudent = user.getRole().contains(Role.STUDENT);
        if(user.getRole().contains(Role.STUDENT))
            allSubjects.removeIf(subject -> !subject.getStudent().equals(principal.getName()));
        else
            allSubjects.removeIf(subject -> !subject.getTeacher().equals(principal.getName()));
        model.addAttribute("subjects", allSubjects);
        model.addAttribute("isStudent", isStudent);
        return "main";
    }
}
