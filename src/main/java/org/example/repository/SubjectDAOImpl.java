package org.example.repository;

import org.example.domain.Subject;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

@Repository
public class SubjectDAOImpl implements SubjectDAO {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public Subject getSubjectById(int id) {
        return sessionFactory.getCurrentSession().get(Subject.class, id);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Subject> getAll() {
        return sessionFactory.getCurrentSession().createQuery("from Subject").list();
    }
}
