package org.example.repository;


import org.example.domain.User;

public interface UserDAO {
    User findUserByUsername(String username);
}
