package org.example.repository;

import org.example.domain.Subject;

import java.util.List;

public interface SubjectDAO {
    Subject getSubjectById(int id);
    List<Subject> getAll();
}
