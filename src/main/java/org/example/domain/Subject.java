package org.example.domain;

import javax.persistence.*;

@Entity
@Table(name="subjects")
public class Subject {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name="subjectName")
    private String subjectName;

    @Column(name="numOfPoints")
    private int numOfPoints;

    @Column(name="reqNumOfPoints")
    private int reqNumOfPoints;

    @Column(name= "student")
    public String student;

    @Column(name="teacher")
    public String teacher;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public int getNumOfPoints() {
        return numOfPoints;
    }

    public void setNumOfPoints(int numOfPoints) {
        this.numOfPoints = numOfPoints;
    }

    public int getReqNumOfPoints() {
        return reqNumOfPoints;
    }

    public void setReqNumOfPoints(int reqNumOfPoints) {
        this.reqNumOfPoints = reqNumOfPoints;
    }

    public String getStudent() {
        return student;
    }

    public void setStudent(String username) {
        this.student = student;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }
}
