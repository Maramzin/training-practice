package org.example.domain;

public enum Role {
    STUDENT,
    TEACHER
}
